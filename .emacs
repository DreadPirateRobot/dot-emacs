(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(auth-source-save-behavior nil)
 '(auto-dim-other-buffers-mode nil)
 '(auto-hscroll-mode t)
 '(avy-keys '(97 115 100 102 106 107 108 59))
 '(aw-display-mode-overlay nil)
 '(column-number-mode nil)
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(compilation-message-face 'default)
 '(custom-enabled-themes nil)
 '(custom-safe-themes t)
 '(display-line-numbers nil)
 '(display-line-numbers-current-absolute nil)
 '(display-time-mode nil)
 '(emms-mode-line-mode-line-function 'emms-mode-line-icon-function)
 '(emms-source-file-default-directory "~/Music")
 '(extended-command-suggest-shorter nil)
 '(fci-rule-color "#eee8d5")
 '(font-lock-global-modes '(not speedbar-mode))
 '(fringe-mode 0 nil (fringe))
 '(global-display-fill-column-indicator-mode nil)
 '(global-display-line-numbers-mode nil)
 '(global-highlight-changes-mode nil)
 '(global-hl-line-mode nil)
 '(global-prettify-symbols-mode t)
 '(global-visual-line-mode nil)
 '(god-mode-alist '((nil . "C-") ("z" . "M-") ("Z" . "C-M-")))
 '(highlight-changes-colors '("#d33682" "#6c71c4"))
 '(highlight-symbol-colors
   '("#efe5da4aafb2" "#cfc5e1add08c" "#fe53c9e7b34f" "#dbb6d3c3dcf4" "#e183dee1b053" "#f944cc6dae48" "#d360dac5e06a"))
 '(highlight-symbol-foreground-color "#586e75")
 '(highlight-tail-colors
   '(("#eee8d5" . 0)
     ("#b3c34d" . 20)
     ("#6ccec0" . 30)
     ("#74adf5" . 50)
     ("#e1af4b" . 60)
     ("#fb7640" . 70)
     ("#ff699e" . 85)
     ("#eee8d5" . 100)))
 '(hl-bg-colors
   '("#e1af4b" "#fb7640" "#ff6849" "#ff699e" "#8d85e7" "#74adf5" "#6ccec0" "#b3c34d"))
 '(hl-fg-colors
   '("#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3"))
 '(hl-paren-colors '("#2aa198" "#b58900" "#268bd2" "#6c71c4" "#859900"))
 '(inhibit-startup-screen t)
 '(initial-major-mode 'org-mode)
 '(initial-scratch-message "")
 '(ivy-mode nil)
 '(jdee-db-active-breakpoint-face-colors (cons "#1b1d1e" "#fc20bb"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1b1d1e" "#60aa00"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1b1d1e" "#505050"))
 '(keypad-numlock-setup 'numeric nil (keypad))
 '(keypad-setup 'cursor nil (keypad))
 '(line-number-mode t)
 '(line-spacing 0)
 '(lsp-ui-doc-border "#586e75")
 '(major-mode 'fundamental-mode)
 '(mastodon-auth-source-file "~/.authinfo")
 '(mastodon-instance-url "https://koyu.space/")
 '(menu-bar-mode nil)
 '(nrepl-message-colors
   '("#dc322f" "#cb4b16" "#b58900" "#5b7300" "#b3c34d" "#0061a8" "#2aa198" "#d33682" "#6c71c4"))
 '(objed-cursor-color "#d02b61")
 '(org-adapt-indentation nil)
 '(org-capture-templates
   '(("t" "todo" checkitem
      (file "~/org/todo.org")
      "")
     ("b" "Things to do on break" checkitem
      (file "~/org/break.org")
      "" :kill-buffer t)))
 '(org-pretty-entities t)
 '(package-selected-packages
   '(nov ace-jump-buffer dictionary sudo-edit make-color svg emacs-everywhere which-key openwith guru-mode keepass-mode emms-player-simple-mpv gameoflife monkeytype centered-cursor-mode darkroom mastodon emojify elpher company vlc slime auto-complete god-mode smartparens web-mode use-package lsp-mode counsel doom-themes emms magit zenburn-theme solarized-theme ace-window swiper expand-region ivy flycheck vscdark-theme rust-playground cargo evil))
 '(pdf-view-midnight-colors (cons "#dddddd" "#1b1d1e"))
 '(pos-tip-background-color "#eee8d5")
 '(pos-tip-foreground-color "#586e75")
 '(recentf-mode t)
 '(rustic-ansi-faces
   ["#1b1d1e" "#d02b61" "#60aa00" "#d08928" "#6c9ef8" "#b77fdb" "#00aa80" "#dddddd"])
 '(scroll-bar-mode nil)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#eee8d5" 0.2))
 '(standard-indent 8)
 '(tab-always-indent t)
 '(term-default-bg-color "#fdf6e3")
 '(term-default-fg-color "#657b83")
 '(tool-bar-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   '((20 . "#dc322f")
     (40 . "#cb4466ec20b5")
     (60 . "#c11679431550")
     (80 . "#b58900")
     (100 . "#a6ae8f7c0000")
     (120 . "#9ed992380000")
     (140 . "#96bf94d00000")
     (160 . "#8e5497440000")
     (180 . "#859900")
     (200 . "#77689bfc4636")
     (220 . "#6d449d475bfe")
     (240 . "#5fc09ea47093")
     (260 . "#4c69a01784aa")
     (280 . "#2aa198")
     (300 . "#303598e7affc")
     (320 . "#2fa1947dbb9b")
     (340 . "#2c889009c736")
     (360 . "#268bd2")))
 '(vc-annotate-very-old-color nil)
 '(visible-cursor t)
 '(weechat-color-list
   '(unspecified "#fdf6e3" "#eee8d5" "#a7020a" "#dc322f" "#5b7300" "#859900" "#866300" "#b58900" "#0061a8" "#268bd2" "#a00559" "#d33682" "#007d76" "#2aa198" "#657b83" "#839496"))
 '(xterm-color-names
   ["#eee8d5" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#073642"])
 '(xterm-color-names-bright
   ["#fdf6e3" "#cb4b16" "#93a1a1" "#839496" "#657b83" "#6c71c4" "#586e75" "#002b36"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t nil)))
 '(aw-mode-line-face ((t (:background "red" :foreground "white" :weight ultra-bold))))
 '(mode-line-inactive ((t nil)))
 '(term ((t (:inherit default))))
 '(term-color-black ((t (:inherit default :background "black" :foreground "black"))))
 '(variable-pitch ((t (:family "Clear sans")))))


(add-hook 'dired-load-hook
(lambda ()
(load "dired-x")
;; Set dired-x global variables here.  For example:
;; (setq dired-guess-shell-gnutar "gtar")
;; (setq dired-x-hands-off-my-keys nil)
))
(add-hook 'dired-mode-hook
(lambda ()
;; Set dired-x buffer-local variables here.  For example:
;; (dired-omit-mode 1)
))

     (autoload 'dired-jump "dired-x"
       "Jump to Dired buffer corresponding to current buffer." t)

     (autoload 'dired-jump-other-window "dired-x"
       "Like \\[dired-jump] (dired-jump) but in other window." t)

     (define-key global-map "\C-x\C-j" 'dired-jump)
     (define-key global-map "\C-x4\C-j" 'dired-jump-other-window)

;(require 'expand-region)
;(ivy-mode 1)
;(helm-mode 1) ;HAHAHAHAHAHAHAHAAH
;(global-set-key "\M-x" 'helm-M-x)
;(global-set-key (kbd "C-x C-f") 'helm-find-files)

(cargo-minor-mode 1)
;(global-set-key (kbd "C-s") 'swiper)
(ace-window-display-mode 1)
(show-paren-mode 1)
;(setq show-paren-delay 0.01)

;(global-set-key (kbd "M-o") 'ace-window)
					;(global-set-key (kbd "C-z") '()) ; something good to rebind, along with C-t, C-o, and C-j (global-set-key (kbd "C-;") 'avy-goto-line)
(global-unset-key (kbd "C-t"))
(global-set-key (kbd "C-c e") 'eww)
(global-set-key (kbd "C-c t") 'eshell)


					;(global-set-key (kbd "C-x b") 'counsel-switch-buffer)
					;(global-set-key (kbd "C-M-t") 'shell)
(global-set-key (kbd "C-=") 'er/expand-region)
;(global-set-key (kbd "C-x C-k") 'kill-buffer)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)


;(require 'god-mode)
;(god-mode)
;(global-set-key (kbd "<escape>") #'god-local-mode)
;(setq god-exempt-major-modes nil)
;(setq god-exempt-predicates nil)

;(defun my-god-mode-update-cursor ()
;  (setq cursor-type (if (or god-local-mode buffer-read-only)
;                        'box
;                      'bar)))

;(add-hook 'god-mode-enabled-hook #'my-god-mode-update-cursor)
;(add-hook 'god-mode-disabled-hook #'my-god-mode-update-cursor)
;(global-set-key (kbd "C-x C-1") #'delete-other-windows)
;(global-set-key (kbd "C-x C-2") #'split-window-below)
;(global-set-key (kbd "C-x C-3") #'split-window-right)
;(global-set-key (kbd "C-x C-0") #'delete-window)
;(global-set-key (kbd "C-x C-b") #'ivy-switch-buffer)
;(add-to-list 'god-exempt-major-modes 'dired-mode)
					;(setq god-mode-enable-function-key-translation nil)
;(smartparens-mode 1)
;(which-key-mode 1)
;(global-set-key (kbd "C-.") 'repeat)
(setq smtpmail-smtp-server "smpt.gmail.com")
(put 'narrow-to-region 'disabled nil)
;(setq blink-cursor-mode nil)
(setq exec-path (append exec-path '("/usr/local/bin")))

;; EMMS configuration
(require 'emms-setup)
(emms-all)
(emms-default-players)
(setq emms-source-file-default-directory "~/Music")

(global-set-key (kbd "C-c x") 'org-capture)
(global-set-key (kbd "C-c a") 'org-agenda)

(put 'scroll-left 'disabled nil)

(global-set-key (kbd "C-<") 'scroll-right)
(global-set-key (kbd "C->") 'scroll-left)
(global-set-key (kbd "M-n") 'next-line)
(global-set-key (kbd "M-p") 'previous-line)

(flyspell-mode 1)

;(global-set-key (kbd "C-t 2") 'tab-new)
;(global-set-key (kbd "C-t 0") 'tab-close)
;(global-set-key (kbd "C-t C-.") 'tab-bar-switch-to-next-tab)
;(global-set-key (kbd "C-t C-,") 'tab-bar-switch-to-prev-tab)
;(global-set-key (kbd "C-t f") 'find-file-other-tab)
;(global-set-key (kbd "C-t C-f") 'find-file-other-tab)
;(global-set-key (kbd "C-t RET") 'tab-bar-select-tab-by-name)
(put 'set-goal-column 'disabled nil)
